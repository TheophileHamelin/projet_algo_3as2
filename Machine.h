//
// Created by theoh on 12/04/2020.
//

#ifndef PROJET_ALGO_MACHINE_H
#define PROJET_ALGO_MACHINE_H


#include <vector>
#include "State.h"

class Machine {
protected:
    std::vector<State *> states;
    std::vector<char> alphabet;
public:
    Machine(const std::vector<State *> &states, const std::vector<char> &alphabet);

    const std::vector<State *> &getStates() const;

    void setStates(const std::vector<State *> &states);

    const std::vector<char> &getAlphabet() const;

    void setAlphabet(const std::vector<char> &alphabet);

    void checkWords(std::string wordsFile, std::string outputFile);

    void checkWordsAFN(std::string wordsFile, std::string outputFile);

    void writeOutputFile(std::string outputFile, char charToWrite);

    void toString();

    virtual ~Machine();
};


#endif //PROJET_ALGO_MACHINE_H
