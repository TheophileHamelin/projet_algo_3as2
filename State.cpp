//
// Created by theoh on 12/04/2020.
//

#include <iostream>
#include "State.h"

State::~State() {
    for(Transition *s : this->transitionsState){
        delete s;
    }
}

bool State::isInitial1() const {
    return isInitial;
}

void State::setIsInitial(bool isInitial) {
    State::isInitial = isInitial;
}

bool State::isFinal1() const {
    return isFinal;
}

void State::setIsFinal(bool isFinal) {
    State::isFinal = isFinal;
}

int State::getName() const {
    return name;
}

void State::setName(int name) {
    State::name = name;
}

State::State(int name) : name(name) {}



void State::setTransitionsState(const std::vector<Transition *> &transitionsState) {
    State::transitionsState = transitionsState;
}

State::State(int name, bool isInitial, bool isFinal, const std::vector<Transition *> &transitionsState) : name(name),
                                                                                                          isInitial(
                                                                                                                  isInitial),
                                                                                                          isFinal(isFinal),
                                                                                                          transitionsState(
                                                                                                                  transitionsState) {}

void State::toString() {
    std::cout<< "Name : " << this->name <<std::endl;
    std::cout<< "Initial : " << this->isInitial <<std::endl;
    std::cout<< "Final : " << this->isFinal <<std::endl;
    std::cout<< "Transitions : " <<std::endl;
    std::vector<Transition*>::iterator itTransitions;

    for(itTransitions=this->transitionsState.begin(); itTransitions!=this->transitionsState.end(); ++itTransitions){
        (*itTransitions)->toString();
    }
}

std::vector<Transition *> State::getTransitionsState() {
    return transitionsState;
}
