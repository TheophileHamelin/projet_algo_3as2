//
// Created by theoh on 12/04/2020.
//

#ifndef PROJET_ALGO_TRANSITION_H
#define PROJET_ALGO_TRANSITION_H


#include <string>

class State; //To fix circular includes

class Transition {
protected:
    char label;
    State *state;
public:
    Transition(char label, State *state);

    char getLabel();

    void setLabel(char label);

    State *getState() const;

    void setState(State *state);

    void toString();

    virtual ~Transition();

};


#endif //PROJET_ALGO_TRANSITION_H
