//
// Created by theoh on 12/04/2020.
//

#include <fstream>
#include <iostream>
#include "MachineParser.h"

Machine *MachineParser::parseMachineFromFile(std::string inputFile) {
    //Input the file
    std::ifstream file(inputFile, std::ios::in);
    // If the file was successfully opened
    if (file) {
        std::string currentLine;
        //A var for the lines parsed from the file
        std::vector<std::string> contents;
        //Get the lines from the file and put the lines in the vector
        while (getline(file, currentLine)) {
            if (currentLine.empty()) continue;
            //Truncate the line if it contains a \r
            if (currentLine[currentLine.size() - 1] == '\r') {
                currentLine.resize(currentLine.size() - 1);
            }
            contents.push_back(currentLine);
        }

        //Alphabet
        std::vector<char> alphabet;
        int i;
        for(i=0; i<contents[0].size(); i++){
            alphabet.push_back(contents[0][i]);
        }

        //Number of states
        int stateNumber = std::stoi(contents[1]);
        std::vector<State*> states;
        for(i=0; i<stateNumber;i++){
            State* s = new State(i);
            states.push_back(s);
        }

        std::vector<Transition*> transitions;
        i=2;
        int transitionNumber = 0;
        int actualState = 0;
        int stateName;
        int stateDest;
        int j;
        std::vector<State*>::iterator itStates;
        //TRANSITIONS + STATES
        while(i<contents.size()){
            //Initial state
            if(contents[i][0] == '1'){
                states[actualState]->setIsInitial(true);
            }else{
                states[actualState]->setIsInitial(false);
            }

            //Final state
            if(contents[i][2] == '1'){
                states[actualState]->setIsFinal(true);
            }else{
                states[actualState]->setIsFinal(false);
            }
            //Next line
            i++;
            transitionNumber = std::stoi(contents[i]);
            for(j=0; j<transitionNumber; j++){
                i++;
                stateDest = contents[i][0] - 48;
                for(itStates = states.begin(); itStates != states.end(); ++itStates){
                    stateName = (*itStates)->getName();

                    if(stateName == stateDest){
                        Transition* t = new Transition(contents[i][2], *itStates);
                        transitions.push_back(t);
                    }
                }
            }
            states[actualState]->setTransitionsState(transitions);
            transitions.clear();

            //NEXT LINE
            i++;
            //CHANGING STATE
            actualState++;
        }
        //Closing file
        file.close();

        //MACHINE CREATION
        Machine* m = new Machine(states, alphabet);
        return m;

    }else{
        std::cout << "File error" << std::endl;
        return nullptr;
    }

}
