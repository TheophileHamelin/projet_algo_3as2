//
// Created by theoh on 12/04/2020.
//

#ifndef PROJET_ALGO_MACHINEPARSER_H
#define PROJET_ALGO_MACHINEPARSER_H


#include "Machine.h"

class MachineParser {
public:
    static Machine* parseMachineFromFile(std::string inputFile);

};


#endif //PROJET_ALGO_MACHINEPARSER_H
