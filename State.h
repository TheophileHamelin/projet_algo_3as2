//
// Created by theoh on 12/04/2020.
//

#ifndef PROJET_ALGO_STATE_H
#define PROJET_ALGO_STATE_H


#include <string>
#include <vector>
#include "Transition.h"

class State {
protected:
    int name;
    bool isInitial;
    bool isFinal;
    std::vector<Transition*> transitionsState;

public:
    State(int name, bool isInitial, bool isFinal, const std::vector<Transition *> &transitionsState);

    State(int name);

    int getName() const;

    void setName(int name);

    bool isInitial1() const;

    void setIsInitial(bool isInitial);

    bool isFinal1() const;

    std::vector<Transition *> getTransitionsState();

    void setTransitionsState(const std::vector<Transition *> &transitionsState);

    void setIsFinal(bool isFinal);

    void toString();

    virtual ~State();
};


#endif //PROJET_ALGO_STATE_H
