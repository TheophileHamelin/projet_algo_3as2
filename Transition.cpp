//
// Created by theoh on 12/04/2020.
//

#include <iostream>
#include "Transition.h"
#include "State.h"

State *Transition::getState() const {
    return state;
}

void Transition::setState(State *state) {
    Transition::state = state;
}

Transition::~Transition() {
    delete state;
}

Transition::Transition(char label, State *state) : label(label), state(state) {}

char Transition::getLabel() {
    return label;
}

void Transition::setLabel(char label) {
    Transition::label = label;
}

void Transition::toString() {
    std::cout<< "(LABEL : " << this->getLabel() << " , STATE : " << this->state->getName() << ")" << std::endl;
}
