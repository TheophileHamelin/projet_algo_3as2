//
// Created by theoh on 12/04/2020.
//

#include <iostream>
#include <fstream>
#include <stack>
#include "Machine.h"


const std::vector<char> &Machine::getAlphabet() const {
    return alphabet;
}

Machine::~Machine() {
    for(State *s : this->states){
        delete s;
    }
}

void Machine::setAlphabet(const std::vector<char> &alphabet) {
    Machine::alphabet = alphabet;
}

Machine::Machine(const std::vector<State *> &states, const std::vector<char> &alphabet) : states(states),
                                                                                          alphabet(alphabet) {}

const std::vector<State *> &Machine::getStates() const {
    return states;
}

void Machine::setStates(const std::vector<State *> &states) {
    Machine::states = states;
}

void Machine::toString() {
    std::cout<< "Alphabet : " <<std::endl;
    std::vector<char>::iterator itAlphabet;
    for(itAlphabet=this->alphabet.begin(); itAlphabet!=this->alphabet.end(); ++itAlphabet){
        std::cout << *itAlphabet << std::endl;
    }
    std::cout<< "States : " <<std::endl;
    std::vector<State*>::iterator itStates;
    for(itStates=this->states.begin(); itStates!=this->states.end(); ++itStates){
        (*itStates)->toString();
    }

}

void Machine::checkWords(std::string wordsFile, std::string outputFile) {
    //Input the file
    std::ifstream file(wordsFile, std::ios::in);
    // If the file was successfully opened
    if (file) {
        std::string currentWord, test;
        //A var for the lines parsed from the file
        std::vector<std::string> words;
        //Get the lines from the file and put the lines in the vector
        while (getline(file, currentWord)) {
            if (currentWord.empty()) continue;
            //Truncate the line if it contains a \r
            if (currentWord[currentWord.size() - 1] == '\r') {
                currentWord.resize(currentWord.size() - 1);
            }
            words.push_back(currentWord);
        }

        int j=0;
        bool wordProcessed;
        int charToTest;
        bool charValidated = false;
        State* currentState;
        std::vector<Transition*>::iterator itTransition;
        char testLabel;
        char testChar;
        std::vector<Transition*> transitions;
        while(j<words.size()){
            currentWord = words[j];
            wordProcessed = false;
            //Check if the first state is initial
            if(!this->states[0]->isInitial1()){
                writeOutputFile(outputFile, '0');
                wordProcessed = true;
            }

            //Variables initialisation
            charToTest=0;
            currentState = this->states[0];

            while(!wordProcessed){
                //For each transitions of the state, I check if there is a label that correspond to the current char
                transitions = currentState->getTransitionsState();
                charValidated=false;

                for(itTransition=transitions.begin(); itTransition != transitions.end(); ++itTransition){
                    //If the label corresponds to the char, the char is validated and the currentState is now the destination state of the transition
                    testLabel= (*itTransition)->getLabel();
                    testChar = currentWord[charToTest];
                    if(testLabel == testChar){
                        currentState = (*itTransition)->getState();
                        charValidated = true;
                    }
                }
                if(charValidated){
                    charToTest++;
                    //If all char of the word have been tested and if the currentState is final, the word is validated
                    if(charToTest == currentWord.size() && currentState->isFinal1()){
                        wordProcessed = true;
                        writeOutputFile(outputFile, '1');
                    }
                }else{
                    //If the first state is not initial => word can be validated
                    wordProcessed = true;
                    writeOutputFile(outputFile, '0');
                }
            }

            j++;
        }
        //Closing file
        file.close();
    }else{
        std::cout << "File error" << std::endl;
    }

}

void Machine::writeOutputFile(std::string outputFile, char charToWrite) {
    //Output file
    std::ofstream file;
    file.open(outputFile ,std::ios_base::app);
    file << charToWrite << "\n";
    file.close();
}

void Machine::checkWordsAFN(std::string wordsFile, std::string outputFile) {
    //Input the file
    std::ifstream file(wordsFile, std::ios::in);
    // If the file was successfully opened
    if (file) {
        std::string currentWord, test;
        //A var for the lines parsed from the file
        std::vector<std::string> words;
        //Get the lines from the file and put the lines in the vector
        while (getline(file, currentWord)) {
            if (currentWord.empty()) continue;
            //Truncate the line if it contains a \r
            if (currentWord[currentWord.size() - 1] == '\r') {
                currentWord.resize(currentWord.size() - 1);
            }
            words.push_back(currentWord);
        }

        int j=0;
        bool wordProcessed;
        int charToTest;
        bool charValidated = false;
        State* currentState;
        std::vector<Transition*>::iterator itTransition;
        char testLabel;
        char testChar;
        std::vector<Transition*> transitions;
        std::stack<Transition*> alternativeTransitions;
        std::stack<int> indexCharacterMemory;

        while(j<words.size()){
            currentWord = words[j];
            wordProcessed = false;
            //Check if the first state is initial
            if(!this->states[0]->isInitial1()){
                writeOutputFile(outputFile, '0');
                wordProcessed = true;
            }

            //Variables initialisation
            charToTest=0;
            currentState = this->states[0];

            while(!wordProcessed){
                //For each transitions of the state, I check if there is a label that correspond to the current char
                transitions = currentState->getTransitionsState();
                charValidated=false;

                for(itTransition=transitions.begin(); itTransition != transitions.end(); ++itTransition){
                    //If the label corresponds to the char, the char is validated and the currentState is now the destination state of the transition
                    testLabel= (*itTransition)->getLabel();
                    testChar = currentWord[charToTest];
                    if(testLabel == testChar){
                        if(!charValidated){
                            currentState = (*itTransition)->getState();
                            charValidated = true;
                        }else{
                            indexCharacterMemory.push(charToTest+1);
                            alternativeTransitions.push((*itTransition));
                        }
                    }
                }
                if(charValidated){
                    charToTest++;
                    //If all char of the word have been tested and if the currentState is final, the word is validated
                    if(charToTest == currentWord.size()){
                        if(currentState->isFinal1()){
                            wordProcessed = true;
                            writeOutputFile(outputFile, '1');
                        }else{
                            if(!alternativeTransitions.empty()){
                                currentState = alternativeTransitions.top()->getState();
                                charToTest = indexCharacterMemory.top();
                                alternativeTransitions.pop();
                                indexCharacterMemory.pop();
                            }else{
                                wordProcessed = true;
                                writeOutputFile(outputFile, '0');
                            }
                        }
                    }
                }else{
                    if(!alternativeTransitions.empty()){
                        currentState = alternativeTransitions.top()->getState();
                        charToTest = indexCharacterMemory.top();
                        alternativeTransitions.pop();
                        indexCharacterMemory.pop();
                    }else{
                        wordProcessed = true;
                        writeOutputFile(outputFile, '0');
                    }

                }
            }
            j++;
        }
        //Closing file
        file.close();
    }else{
        std::cout << "File error" << std::endl;
    }
}
